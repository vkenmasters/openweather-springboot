package mx.com.project.weather.callers.test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.github.tomakehurst.wiremock.WireMockServer;

import mx.com.project.exception.WeatherException;
import mx.com.project.weather.callers.CityWeatherCaller;
import mx.com.project.weather.json.OpenWeather;
import mx.com.project.weather.request.RequestCityWeather;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations="classpath:application-dev.properties")
public class CityWeatherCallerTest {

	static WireMockServer wireMockServer;

	@Autowired
	private CityWeatherCaller caller;

	@BeforeEach
    public void setup () {
        wireMockServer = new WireMockServer(8081);
        wireMockServer.start();
        setupStub();
    }
 
    @AfterEach
    public void stopServer () {
        wireMockServer.stop();
    }

    public String generateJsonResponse() {
    	return "{\"coord\": { \"lon\": 139,\"lat\": 35},\r\n" + 
    			"  \"weather\": [\r\n" + 
    			"    {\r\n" + 
    			"      \"id\": 800,\r\n" + 
    			"      \"main\": \"Clear\",\r\n" + 
    			"      \"description\": \"clear sky\",\r\n" + 
    			"      \"icon\": \"01n\"\r\n" + 
    			"    }\r\n" + 
    			"  ],\r\n" + 
    			"  \"base\": \"stations\",\r\n" + 
    			"  \"main\": {\r\n" + 
    			"    \"temp\": 289.92,\r\n" + 
    			"    \"pressure\": 1009,\r\n" + 
    			"    \"humidity\": 92,\r\n" + 
    			"    \"temp_min\": 288.71,\r\n" + 
    			"    \"temp_max\": 290.93\r\n" + 
    			"  },\r\n" + 
    			"  \"wind\": {\r\n" + 
    			"    \"speed\": 0.47,\r\n" + 
    			"    \"deg\": 107.538\r\n" + 
    			"  },\r\n" + 
    			"  \"clouds\": {\r\n" + 
    			"    \"all\": 2\r\n" + 
    			"  },\r\n" + 
    			"  \"dt\": 1560350192,\r\n" + 
    			"  \"sys\": {\r\n" + 
    			"    \"type\": 3,\r\n" + 
    			"    \"id\": 2019346,\r\n" + 
    			"    \"message\": 0.0065,\r\n" + 
    			"    \"country\": \"JP\",\r\n" + 
    			"    \"sunrise\": 1560281377,\r\n" + 
    			"    \"sunset\": 1560333478\r\n" + 
    			"  },\r\n" + 
    			"  \"timezone\": 32400,\r\n" + 
    			"  \"id\": 1851632,\r\n" + 
    			"  \"name\": \"London\",\r\n" + 
    			"  \"cod\": 200\r\n" + 
    			"}";
    }

    public void setupStub() {
        wireMockServer.stubFor(get(urlEqualTo("/data/2.5/weather?q=London&APPID=bc640d2ffd449ae5f55eff67b7984336&units=metric"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBody(generateJsonResponse())));
        
        wireMockServer.stubFor(get(urlEqualTo("/data/2.5/weather?q=Londor&APPID=bc640d2ffd449ae5f55eff67b7984336&units=metric"))
                .willReturn(aResponse().withStatus(404)
                        .withBody(generateJsonResponse())));
    }

	@Test
	public void testCallOpenWeatherApiCorrect() {
		RequestCityWeather request = new RequestCityWeather("London");
		OpenWeather result = caller.callOpenWeatherApi(request);
		assertNotNull(result);
		assertEquals("London",result.getName());
	}

	@Test
	public void testCallOpenWeatherApiError() {
		RequestCityWeather request = new RequestCityWeather("Londor");
		assertThrows(WeatherException.class, 
				() -> caller.callOpenWeatherApi(request));
	}

}
