package mx.com.project.weather.converters.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import mx.com.project.exception.WeatherException;
import mx.com.project.weather.converters.TimeZoneConverter;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class TimeZoneConverterTest {

	@Autowired
	private TimeZoneConverter converter;

	@Test
	public void testConvertDateToTimeZoneOpenApi() {
		Long seconds = 969755700000l;
		String city = "London";
		String time = converter.convertDateToTimeZoneOpenApi(seconds, city);
		assertEquals("08:20 AM", time);
	}

	@Test
	public void testConvertDateToTimeZoneOpenApiNullMiliseconds() {
		String city = "London";
		assertThrows(WeatherException.class, 
					() -> converter.convertDateToTimeZoneOpenApi(null, city));
	}

	@Test
	public void testConvertDateToTimeZoneOpenApiNulCity() {
		Long miliseconds = 969755700000l;
		assertThrows(WeatherException.class, 
					() -> converter.convertDateToTimeZoneOpenApi(miliseconds, null));
	}

}
