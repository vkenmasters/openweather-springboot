package mx.com.project.weather.converters.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import mx.com.project.exception.WeatherException;
import mx.com.project.weather.converters.DegreesConverter;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class DegreesConverterTest {

	@Autowired
	DegreesConverter degreesConverter;

	@Test
	public void convertCelsiusToFahrenheitTest() {
		BigDecimal celsiusDegrees = new BigDecimal("87");
		BigDecimal expectedDegrees = new BigDecimal("188.60");
		assertEquals(expectedDegrees,
				degreesConverter.convertCelsiusToFahrenheit(celsiusDegrees));
	}

	@Test
	public void convertCelsiusToFahrenheitTestNull() {
		assertThrows(WeatherException.class, 
				() -> degreesConverter.convertCelsiusToFahrenheit(null));
	}

}
