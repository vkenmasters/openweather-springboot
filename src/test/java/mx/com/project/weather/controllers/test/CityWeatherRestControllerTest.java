package mx.com.project.weather.controllers.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import mx.com.project.weather.request.RequestCityWeather;
import mx.com.project.weather.response.ResponseCityWeather;
import mx.com.project.weather.service.CityWeatherService;
import mx.com.project.weather.vo.CityWeather;

@SpringBootTest (webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class CityWeatherRestControllerTest {

	@MockBean
    private CityWeatherService cityWeatherService;
	
	@Autowired
	private MockMvc mvc;
	
	public ResponseCityWeather generateOkCityWeatherVO() {
		CityWeather cityWeather = new CityWeather();
		cityWeather.setCelsiusDegrees("30");
		cityWeather.setCityName("London");
		cityWeather.setFahrenheitDegrees("47.54");
		cityWeather.setOverallDescription("small breeze");
		cityWeather.setSunriseTime("8:00 am");
		cityWeather.setSunsetTime("6:00 pm");
		cityWeather.setTodayDate("11/07/2019");
		ResponseCityWeather response = new ResponseCityWeather(true, null, cityWeather);
		return response;
	}

	public ResponseCityWeather generateErrorCityWeatherVO() {
		ResponseCityWeather response = new ResponseCityWeather(false, "Error at connecting", null);
		return response;
	}

    @Test
    public void testGetCityWeatherSuccess() throws Exception {
    	RequestCityWeather request = new RequestCityWeather();
		request.setCityName("London");
        when(cityWeatherService.getWeatherByCity(request))
                .thenReturn(generateOkCityWeatherVO());
    	
    	ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(new RequestCityWeather("London"));
    	
    	mvc.perform( MockMvcRequestBuilders
    		      .post("/weather/request")
    		      .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
    		      .content(json)
    		      .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
    			  .andDo(print())
    		      .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(true))
    		      .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage").isEmpty())
    		      .andExpect(MockMvcResultMatchers.jsonPath("$.cityWeather").isNotEmpty());
    }

    @Test
    public void testGetCityWeatherError() throws Exception {
    	RequestCityWeather request = new RequestCityWeather();
		request.setCityName("Londor");
        when(cityWeatherService.getWeatherByCity(request))
                .thenReturn(generateErrorCityWeatherVO());
    	
    	ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(new RequestCityWeather("Londor"));
    	
    	mvc.perform( MockMvcRequestBuilders
    		      .post("/weather/request")
    		      .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
    		      .content(json)
    		      .accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
    			  .andDo(print())
    		      .andExpect(MockMvcResultMatchers.jsonPath("$.success").value(false))
    		      .andExpect(MockMvcResultMatchers.jsonPath("$.errorMessage").isNotEmpty())
    		      .andExpect(MockMvcResultMatchers.jsonPath("$.cityWeather").isEmpty());
    }

}
