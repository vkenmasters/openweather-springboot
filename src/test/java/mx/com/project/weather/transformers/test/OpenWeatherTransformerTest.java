package mx.com.project.weather.transformers.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import mx.com.project.exception.WeatherException;
import mx.com.project.weather.json.Main;
import mx.com.project.weather.json.OpenWeather;
import mx.com.project.weather.json.Sys;
import mx.com.project.weather.json.Weather;
import mx.com.project.weather.transformers.OpenWeatherTransformer;
import mx.com.project.weather.vo.CityWeather;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class OpenWeatherTransformerTest {

	@Autowired
	private OpenWeatherTransformer transformer;

	private OpenWeather generateValidOpenWeatherObject() {
		OpenWeather result = new OpenWeather();
		result.setName("London");
		
		Main main = new Main();
		main.setTemp(new BigDecimal("23"));
		result.setMain(main);

		Sys sys = new Sys();
		sys.setSunrise(969755700000l);
		sys.setSunset(969714000000l);
		result.setSys(sys);

		List<Weather> weathers = new ArrayList<Weather>();
		Weather weather1 = new Weather();
		weather1.setDescription("clear sky");
		Weather weather2 = new Weather();
		weather2.setDescription("light breeze");
		weathers.add(weather1);
		weathers.add(weather2);

		result.setWeather(weathers);

		return result;
	}

	@Test
	public void testTrasnformOpenWeatherToCityWeather() {
		CityWeather result = transformer.transformOpenWeatherToCityWeather(
											generateValidOpenWeatherObject());
		
		assertEquals("London", result.getCityName());
		assertEquals("clear sky,light breeze", result.getOverallDescription());
		assertEquals("08:20 AM", result.getSunriseTime());
		assertEquals("04:00 PM", result.getSunsetTime());
		assertEquals("23", result.getCelsiusDegrees());
		assertEquals("73.40", result.getFahrenheitDegrees());
	}

	@Test
	public void testTransformOpenWeatherToCityWeather_NullInsteadOfObject() {
		assertThrows(WeatherException.class, 
				() -> transformer.transformOpenWeatherToCityWeather(null));
	}

}
