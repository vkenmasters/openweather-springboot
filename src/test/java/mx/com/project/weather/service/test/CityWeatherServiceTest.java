package mx.com.project.weather.service.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import mx.com.project.exception.WeatherException;
import mx.com.project.weather.callers.CityWeatherCaller;
import mx.com.project.weather.json.Main;
import mx.com.project.weather.json.OpenWeather;
import mx.com.project.weather.json.Sys;
import mx.com.project.weather.json.Weather;
import mx.com.project.weather.request.RequestCityWeather;
import mx.com.project.weather.response.ResponseCityWeather;
import mx.com.project.weather.service.CityWeatherService;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class CityWeatherServiceTest {

	@InjectMocks
	@Autowired
	CityWeatherService cityWeatherService;

	@Mock
	private CityWeatherCaller cityWeatherCaller;

	@BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

	private OpenWeather generateValidOpenWeatherObject() {
		OpenWeather result = new OpenWeather();
		result.setName("London");
		
		Main main = new Main();
		main.setTemp(new BigDecimal("23"));
		result.setMain(main);

		Sys sys = new Sys();
		sys.setSunrise(969714000000l);
		sys.setSunset(969755700000l);
		result.setSys(sys);

		List<Weather> weathers = new ArrayList<Weather>();
		Weather weather1 = new Weather();
		weather1.setDescription("clear sky");
		Weather weather2 = new Weather();
		weather2.setDescription("light breeze");
		weathers.add(weather1);
		weathers.add(weather2);

		result.setWeather(weathers);

		return result;
	}

	@Test
	public void getCityWeatherTest() {
		RequestCityWeather request = new RequestCityWeather("London");
		Mockito.when(cityWeatherCaller.callOpenWeatherApi(request))
		.thenReturn(generateValidOpenWeatherObject());

		ResponseCityWeather weather = cityWeatherService.getWeatherByCity(request);
		assertNotNull(weather);
		assertEquals(true, weather.getSuccess());
		assertEquals(null, weather.getErrorMessage());
		assertNotNull(weather.getCityWeather());
	}

	@Test
	public void getCityWeatherTestNotResponse() {
		RequestCityWeather request = new RequestCityWeather("Londor");
		Mockito.when(cityWeatherCaller.callOpenWeatherApi(request))
		.thenThrow(new WeatherException("Error at calling the Open Weather API"));

		ResponseCityWeather weather = cityWeatherService.getWeatherByCity(request);
		assertNotNull(weather);
		assertEquals(weather.getSuccess(), false);
		assertNotNull(weather.getErrorMessage());
		assertNull(weather.getCityWeather());
	}

}
