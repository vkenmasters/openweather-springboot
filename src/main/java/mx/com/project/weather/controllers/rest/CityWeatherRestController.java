package mx.com.project.weather.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.project.weather.request.RequestCityWeather;
import mx.com.project.weather.response.ResponseCityWeather;
import mx.com.project.weather.service.CityWeatherService;

@RestController
public class CityWeatherRestController {

	public static final String GET_CITY_WEATHER = "/weather/request";
	
	@Autowired
	private CityWeatherService cityWeatherService;

	@RequestMapping(method = RequestMethod.POST, value=GET_CITY_WEATHER)
	@ResponseBody
	public ResponseCityWeather getCityWeather(@RequestBody RequestCityWeather request) {
		return cityWeatherService.getWeatherByCity(request);
	}
}
