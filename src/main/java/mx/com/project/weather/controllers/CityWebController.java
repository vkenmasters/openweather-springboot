package mx.com.project.weather.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CityWebController {

	@RequestMapping("/")
    public String viewHome() {
        return "index";
    }

}
