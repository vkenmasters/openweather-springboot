package mx.com.project.weather.converters;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;
import mx.com.project.exception.WeatherException;

@Service
@Log4j2
public class DegreesConverter {

	private final static float fahrenheitMultiplier = 1.8f;
	private final static int fahrenheitFactor = 32;

	/**
	* Converts Celsius Degrees to Fahrenheit Degrees
	*
	* @author  Salvador Gonzalez
	* @version 1.0
	* @since   2019-06-18 
	*/
	public BigDecimal convertCelsiusToFahrenheit(BigDecimal celsiusDegrees) throws WeatherException{
		
		BigDecimal fahrenheitDegrees = null;
		String errorMessage = "";
		
		try {
			fahrenheitDegrees = celsiusDegrees
								.multiply(new BigDecimal(fahrenheitMultiplier))
								.add(new BigDecimal(fahrenheitFactor))
								.setScale(2, RoundingMode.HALF_UP);
		}catch(Exception ex) {
			errorMessage = "An error ocurred while converting the degrees from celsius to fahrenheit";
			log.error(errorMessage,ex);
			throw new WeatherException(errorMessage,ex);
		}
		return fahrenheitDegrees;
	}

}
