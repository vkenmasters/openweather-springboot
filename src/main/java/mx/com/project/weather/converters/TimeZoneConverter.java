package mx.com.project.weather.converters;

import java.time.DateTimeException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;
import mx.com.project.exception.WeatherException;

@Service
@Log4j2
public class TimeZoneConverter {

	@Value("${date.format.hour}")
	private String hourFormat;

	public String convertDateToTimeZoneOpenApi(Long currentTime, String city) {
	
		String timeZone = "";
		ZonedDateTime timezoneTime = null;
		String errorMessage = "";

		try {
			
			switch(city) {
			case "London":
				timeZone = "Europe/London";
				break;
			case "Hong Kong":
				timeZone = "Asia/Hong_Kong";
				break;
			}
		
		
		timezoneTime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(currentTime), 
									 ZoneId.of(timeZone));
		
		timeZone = timezoneTime.format(DateTimeFormatter.ofPattern(hourFormat)
					.withZone(ZoneId.of(timeZone)));
		}catch(NullPointerException | DateTimeException ex) {
			errorMessage = "Error at setting the correct Time Zone";
			log.error(errorMessage,ex);
			throw new WeatherException(errorMessage,ex);
		}
		
		return timeZone;
	}

}
