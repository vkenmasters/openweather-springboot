package mx.com.project.weather.callers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.log4j.Log4j2;
import mx.com.project.exception.WeatherException;
import mx.com.project.weather.json.OpenWeather;
import mx.com.project.weather.request.RequestCityWeather;

@Service
@Log4j2
public class CityWeatherCaller {

	@Value("${weather.apiKey}")
	private String apiKey;
	@Value("${weather.url}")
	private String weatherUrl;
	@Value("${weather.url.param.city}")
	private String paramCity;
	@Value("${weather.url.param.apikey}")
	private String paramApiKey;
	@Value("${weather.url.param.units}")
	private String paramUnits;
	@Value("${weather.url.param.units.celsius}")
	private String celsiusUnits;

	public OpenWeather callOpenWeatherApi(RequestCityWeather request){
		OpenWeather result = null;
		RestTemplate restTemplate = new RestTemplate();
		String errorMessage = "";
		try {
			StringBuilder fullUrl = new StringBuilder(weatherUrl);
			fullUrl.append(paramCity);
			fullUrl.append(request.getCityName());
			fullUrl.append("&");
			fullUrl.append(paramApiKey);
			fullUrl.append(apiKey);
			fullUrl.append("&");
			fullUrl.append(paramUnits);
			fullUrl.append(celsiusUnits);

			result = restTemplate.getForObject(fullUrl.toString(), OpenWeather.class);
		}catch(RestClientException ex) {
			errorMessage = "Error at calling the Open Weather API";
			log.error(errorMessage,ex);
			throw new WeatherException(errorMessage,ex);
		}
		return result;
	}

}
