package mx.com.project.weather.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import mx.com.project.weather.vo.CityWeather;

@Getter @Setter @AllArgsConstructor
public class ResponseCityWeather {

	private Boolean success;
	private String errorMessage;
	private CityWeather cityWeather;

}
