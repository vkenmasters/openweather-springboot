package mx.com.project.weather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.project.exception.WeatherException;
import mx.com.project.weather.callers.CityWeatherCaller;
import mx.com.project.weather.json.OpenWeather;
import mx.com.project.weather.request.RequestCityWeather;
import mx.com.project.weather.response.ResponseCityWeather;
import mx.com.project.weather.transformers.OpenWeatherTransformer;
import mx.com.project.weather.vo.CityWeather;

@Service
public class CityWeatherService {
	
	@Autowired
	private CityWeatherCaller apiCaller;	
	@Autowired
	private OpenWeatherTransformer objectConverter;

	public ResponseCityWeather getWeatherByCity(RequestCityWeather request) {
		ResponseCityWeather result = null;
		
		try {
			OpenWeather jsonObject = apiCaller.callOpenWeatherApi(request);
			CityWeather realObject = objectConverter.transformOpenWeatherToCityWeather(jsonObject);			
			result = new ResponseCityWeather(true, null, realObject);
		}catch(WeatherException ex) {
			result = new ResponseCityWeather(false, ex.getMessage(), null);
		}
		
		return result;
	}
}
