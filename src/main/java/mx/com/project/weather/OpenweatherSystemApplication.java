package mx.com.project.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenweatherSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenweatherSystemApplication.class, args);
	}

}
