package mx.com.project.weather.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class CityWeather {

	private String todayDate;
	private String cityName;
	private String overallDescription;
	private String fahrenheitDegrees;
	private String celsiusDegrees;
	private String sunriseTime;
	private String sunsetTime;

}
