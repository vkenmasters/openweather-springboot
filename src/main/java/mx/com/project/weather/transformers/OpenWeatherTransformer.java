package mx.com.project.weather.transformers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mx.com.project.exception.WeatherException;
import mx.com.project.weather.converters.DegreesConverter;
import mx.com.project.weather.converters.TimeZoneConverter;
import mx.com.project.weather.json.OpenWeather;
import mx.com.project.weather.json.Weather;
import mx.com.project.weather.vo.CityWeather;

@Service
public class OpenWeatherTransformer {

	@Value("${date.format.standard}")
	private String standardDateFormat;

	@Autowired
	TimeZoneConverter timeConverter;
	@Autowired
	private DegreesConverter degreesConverter;

	public CityWeather transformOpenWeatherToCityWeather(OpenWeather jsonObject) throws WeatherException{
		CityWeather result = new CityWeather();
		SimpleDateFormat standard = new SimpleDateFormat(standardDateFormat);
		String errorMessage = "";
		
		if(jsonObject == null) {
			errorMessage = "The open weather json object cannot be empty";
			throw new WeatherException(errorMessage);
		}
		
		result.setCityName(jsonObject.getName());
		result.setTodayDate(standard.format(Calendar.getInstance().getTime()));
		
		result.setCelsiusDegrees(jsonObject.getMain().getTemp().toString());

		result.setFahrenheitDegrees(degreesConverter
				.convertCelsiusToFahrenheit(jsonObject.getMain().getTemp())
				.toString());

		result.setSunriseTime(timeConverter
							.convertDateToTimeZoneOpenApi(jsonObject.getSys().getSunrise(), 
									jsonObject.getName()));

		result.setSunsetTime(timeConverter
				.convertDateToTimeZoneOpenApi(jsonObject.getSys().getSunset(), 
						jsonObject.getName()));

		String weatherDescription = jsonObject.getWeather().stream()
									.map(Weather::getDescription)
									 .collect(Collectors.joining(","));
		result.setOverallDescription(weatherDescription);
		return result;
	}	

}
