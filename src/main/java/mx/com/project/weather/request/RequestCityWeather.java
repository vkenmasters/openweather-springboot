package mx.com.project.weather.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode
public class RequestCityWeather implements Serializable{

	private static final long serialVersionUID = -6871247179747303129L;

	private String cityName;

}
