package mx.com.project.weather.json;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Sys implements Serializable{

	private static final long serialVersionUID = 8289757143014204566L;

	private Integer type;
    private Long id;
    private BigDecimal message;
    private String country;
    private Long sunrise;
    private Long sunset;

}
