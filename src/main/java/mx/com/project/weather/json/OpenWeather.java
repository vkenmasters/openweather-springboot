package mx.com.project.weather.json;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class OpenWeather implements Serializable{

	private static final long serialVersionUID = -7893591257060129136L;

	private Long id;
	private String name;
	private String base;
	private Long dt;
	private Long timezone;
	private Integer cod;

	private List<Weather> weather;
	private Main main;
	private Wind wind;
	private Cloud clouds;
	private Sys sys;

}
