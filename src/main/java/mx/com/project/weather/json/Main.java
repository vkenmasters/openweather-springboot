package mx.com.project.weather.json;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Main implements Serializable{

	private static final long serialVersionUID = -8358194921185508323L;

	private BigDecimal temp;
	private BigDecimal pressure;
	private BigDecimal humidity;
	private BigDecimal tempMin;
	private BigDecimal tempMax;

}
