package mx.com.project.weather.json;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Wind implements Serializable{

	private static final long serialVersionUID = 9071809190342429845L;

	private BigDecimal speed;
	private BigDecimal deg;

}
