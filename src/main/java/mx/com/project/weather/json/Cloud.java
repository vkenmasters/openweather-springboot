package mx.com.project.weather.json;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Cloud implements Serializable{

	private static final long serialVersionUID = 4353905307198890360L;

	private Integer all;

}
