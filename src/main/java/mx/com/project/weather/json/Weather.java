package mx.com.project.weather.json;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Weather implements Serializable{

	private static final long serialVersionUID = -6452880695253169415L;

	private Long id;
	private String main;
	private String description;
	private String icon;

}
