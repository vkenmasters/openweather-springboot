package mx.com.project.weather.json;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
public class Coord implements Serializable{

	private static final long serialVersionUID = -5838536935908309584L;

	private Integer lon;
	private Integer lat;

}
