package mx.com.project.exception;

public class WeatherException extends RuntimeException {

	private static final long serialVersionUID = -4113966520836308184L;

	public WeatherException(Throwable cause) {
		super(cause);
	}

	public WeatherException(String message, Throwable cause) {
		super(message, cause);
	}

	public WeatherException(String message) {
		super(message);
	}

	public WeatherException() {
		super();
	}
}
