To run this project:

- Use a console or terminal and navigate to the project folder
- Run the following command: mvn spring-boot:run
- Open a browser and go to the following url: http://localhost:8080/open-weather/